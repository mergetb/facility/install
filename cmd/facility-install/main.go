package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/facility/install/pkg/install"
	"gitlab.com/mergetb/tech/shared/install/cache"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func main() {

	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: true,
	})

	root := &cobra.Command{
		Use:   "facility-install",
		Short: "Merge facility installer",
	}

	var zinUpdate bool
	var iptNft bool
	var userConfig string
	var cacheFile string

	generate := &cobra.Command{
		Use:   "generate <facility.xir>",
		Short: "Generate bootable install specs from facility XIR topology",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { generate(args[0], zinUpdate, iptNft, userConfig, cacheFile) },
	}

	generate.PersistentFlags().BoolVarP(&zinUpdate, "zincati", "z", false, "Enable Zincati updates")
	generate.PersistentFlags().BoolVarP(&iptNft, "iptables-nft", "i", false, "Use iptables-nft backend instead of iptables-legacy on FCOS nodes (EXPERIMEINTAL, MAY NOT WORK)")
	generate.PersistentFlags().StringVarP(&userConfig, "config", "c", "user-config.yml", "User configuration file")
	generate.PersistentFlags().StringVarP(&cacheFile, "cache", "j", "cache.json", "node cache file")

	root.AddCommand(generate)

	root.Execute()

}

func generate(sourcefile string, zinUpdate, nft bool, userconfig, cachefn string) {

	config, err := install.GetConfig(userconfig)
	if err != nil {
		log.Fatalf("get config: %v", err)
	}

	cache, err := cache.NewCache(cachefn)
	if err != nil {
		log.Fatalf("new cache: %v", err)
	}

	err = install.CertInit(config)
	if err != nil {
		log.Fatalf("cert init: %v", err)
	}

	tbx, err := xir.FacilityFromFile(sourcefile)
	if err != nil {
		log.Fatalf("read facility: %v", err)
	}

	err = install.Genspec(tbx, config, zinUpdate, nft, cache)
	if err != nil {
		log.Fatalf("genspec: %v", err)
	}

	log.Infof("writing cache %s", cachefn)
	err = cache.Save(cachefn)
	if err != nil {
		log.Fatalf("save cache: %v", err)
	}
}
