#!/usr/bin/env bash
set -x

# default to pwd (should be the name of the git repo)
PKG_NAME=${1:-`basename $PWD`}
# create default rpmbuild directory structure
mkdir -vp ~/rpmbuild/SOURCES

# if we gitlab CI var is set, lets automate.
if [[ -n "$CI" ]]
then
    VERSION=${CI_COMMIT_TAG:-$LASTVERSION}
    if [[ "$CI_COMMIT_TAG" == "${LASTVERSION}" ]]
    then
        RELEASE=$((LASTRELEASE+1))
    else
        if [[ "$CI_COMMIT_BRANCH" == "main" ]] || [[ "$CI_COMMIT_BRANCH" == "master" ]]
        then
            RELEASE=$((LASTRELEASE+1))
        else
            RELEASE=${CI_COMMIT_BRANCH:-"1"}
            # remove dashes from release name
            RELEASE=`echo $RELEASE | tr -d '-'`
        fi
    fi
    # -s == file is not zero size, ! -s == file should be zero size
    if [[ ! -s ~/rpmbuild/SOURCES/${PKG_NAME}-${VERSION}.tar.gz ]]
    then
        # create the tarball from local files instead of pulling from web
        ln -sv ./ ${PKG_NAME}-${VERSION}
        tar --exclude=${PKG_NAME}-${VERSION}/${PKG_NAME}-${VERSION} --exclude=.git --exclude=rpm \
            -cvzf ~/rpmbuild/SOURCES/${PKG_NAME}-${VERSION}.tar.gz ${PKG_NAME}-${VERSION}/*
        rm -v ./${PKG_NAME}-${VERSION}
    fi
fi

[[ -z "$VERSION" ]] && echo "You MUST export variable VERSION to build the rpm with this script!" && exit 1
# rpmbuild does not like non-existent file, so touch it, in case we are not in CI mode
touch ~/rpmbuild/SOURCES/${PKG_NAME}-${VERSION}.tar.gz
# this specific build requires jumping through hoops due to repository naming issues...
touch ~/rpmbuild/SOURCES/install-${VERSION}.tar.gz

[[ -z "$RELEASE" ]] && echo "You MUST export variable RELEASE to build the rpm with this script!" && exit 1
[[ -z "$PKG_NAME" ]] && echo "Please set the package name variable PKG_NAME (for \$PKG_NAME.spec)."

echo "Building RPM based on this info:"
echo " PACKAGE NAME=${PKG_NAME}"
echo " VERSION=${VERSION}"
echo " RELEASE=${RELEASE}"
ARCH=${ARCH:-`arch`}
echo " ARCH=`arch`"

if yum-builddep -h >/dev/null
then
    yum-builddep -y rpm/${PKG_NAME}.spec
fi
rpmbuild --verbose --verbose --define "_version   ${VERSION}" --define "_release   ${RELEASE}" --clean -ba rpm/${PKG_NAME}.spec || exit 1

# setting up the files for CI deployment to pkg:/var/www/repo/mergetb/rpms/${CUR_OS}/${OS_VER}/${ARCH}
if [[ -n "$CI" ]]
then
    # just make sure build artifact directory exists
    [[ -d build ]] || mkdir -v build
    if [[ -n "${CUR_OS}" ]] && [[ -n "${OS_VER}" ]] && [[ -n "${ARCH}" ]]
    then
        BUILTRPMNAME=`find ~/rpmbuild/RPMS/ -name \*rpm -exec basename {} \;`
        ULFILE=${BUILTRPMNAME}.ulpath
        echo "/var/www/repo/mergetb/rpms/${CUR_OS}/${OS_VER}/${ARCH}/" > build/$ULFILE
    fi
    find ~/rpmbuild/RPMS -name \*rpm -exec cp -v {} build/ \;
    ls -lh build
fi
