Name:           facility-install
Version:        %{_version}
Release:        %{_release}%{?dist}
Summary:        The Merge facility installer.

License:        ASL 2.0
URL:            https://gitlab.com/mergetb/facility/install
Source0:        https://gitlab.com/mergetb/facility/install/-/releases/%{version}/install-%{version}.tar.gz
BuildRequires:  golang make
# ymk is a mergetb ecosystem specific build tool

# this undefine fixes expectation of debugger entities
%global debug_package %{nil}
%define _build_id_links none

%description
The MergeTB facility-install program will convert your MergeTB
  facility models into functional configurations to begin deploying
  a MergeTB facility, including ground-control configuration,
  CumulusLinux ZTP configuration, and Fedora CoreOS ignition files.

# -s == file is not zero size, ! -s == file should be zero size
%prep
mkdir -vp %{_sourcedir}
pushd %{_sourcedir}
ls -lh
# if we don't have an available tarball, shuffle things around
if [[ ! -f %{name}-%{version}.tar.gz ]]
then
  [[ -s install-%{version}.tar.gz ]] || curl -sO %{url}/-/archive/%{version}/install-%{version}.tar.gz
  # and rename it (ugh)
  tar -xvof install-%{version}.tar.gz
  mv -v install-%{version} %{name}-%{version}
  tar -cvzf %{name}-%{version}.tar.gz %{name}-%{version}
  ln -vs %{name}-%{version}.tar.gz install-%{version}.tar.gz
else
  mv %{name}-%{version}.tar.gz install-%{version}.tar.gz
fi
popd
%autosetup

%build
# output the current directory & build if binary does not yet exist
# the ymk binary/artifact may already exist in the gitlab CI pipeline
echo "PWD and directory listing" && pwd && ls -lh
[[ -e build/facility-install ]] || make
# TODO: Documentation

%install
rm -rf $RPM_BUILD_ROOT
install -v -d -m 0755 %{buildroot}/%{_bindir}/
install -v -m 0755 build/* %{buildroot}/%{_bindir}/


%files
%{_bindir}/*
%license LICENSE
%doc README.md


%changelog
#TODO: automate change inclusion since last approved MR
