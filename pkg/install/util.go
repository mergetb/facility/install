package install

import (
	"encoding/binary"
	"fmt"
	"net"
)

func firstip4(subnet string) (string, error) {

	_, ipnet, err := net.ParseCIDR(subnet)
	if err != nil {
		return "", fmt.Errorf("parse CIDR %s: %v", subnet, err)
	}

	return ipnet.IP.To4().String(), nil
}

func incip4(addr string) (string, error) {

	return incip4By(addr, 1)

}

func incip4By(addr string, inc uint32) (string, error) {

	ip := net.ParseIP(addr)
	if ip == nil {
		return "", fmt.Errorf("invalid ip %s", addr)
	}

	x := binary.BigEndian.Uint32(ip.To4()) + inc
	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, x)
	return net.IP(buf).String(), nil

}
