package install

type PodmanServiceTemplateArgs struct {
	Name           string
	Image          string
	RunOptions     []string
	Args           []string
	After          []string
	Wants          []string
	Requires       []string
	PartOf         []string
	BindsTo        []string
	PullOptions    []string
	UnitOptions    []string
	ServiceOptions []string
}

const podmanServiceTemplate = `[Unit]
Description={{.Name}}
Wants=network-online.target
{{- range $index, $item := .Wants}}
Wants={{$item}}
{{- end}}
After=network-online.target
{{- range $index, $item := .After}}
After={{$item}}
{{- end}}
{{- range $index, $item := .PartOf}}
PartOf={{$item}}
{{- end}}
{{- range $index, $item := .Requires}}
Requires={{$item}}
{{- end}}
{{- range $index, $item := .BindsTo}}
BindsTo={{$item}}
{{- end}}
{{- range $index, $item := .UnitOptions}}
{{$item}}
{{- end}}

[Service]
Restart=always
RestartSec=5s
{{- range $index, $item := .ServiceOptions}}
{{$item}}
{{- end}}
ExecStartPre=/usr/bin/podman pull {{range $index, $opt := .PullOptions}} {{$opt}} {{end}} {{.Image}}
ExecStartPre=/usr/bin/podman rm -f --ignore {{.Name}}
ExecStartPre=/usr/bin/rm -f %t/%n-pid %t/%n-cid
ExecStart=/usr/bin/podman run \
    --log-driver journald \
    --conmon-pidfile %t/%n-pid \
    --cidfile %t/%n-cid \
    --name {{.Name}} --rm \
    {{range $index, $opt := .RunOptions -}} {{$opt}} {{end}} \
    -d {{.Image}} \
    {{range $index, $arg := .Args}} {{$arg}} {{end}}
ExecStop=/usr/bin/rm -f %t/%n-pid %t/%n-cid
TimeoutStartSec=300
TimeoutStopSec=60
Type=forking
PIDFile=%t/%n-pid

[Install]
WantedBy=multi-user.target
`

var podmanHostOpts = []string{
	"--network=host",
	"--privileged",
}

type DiskFormatTemplateArgs struct {
	Dev string
}

const diskFormatTemplate = `[Unit]
Description=Format {{.Dev}} disk

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/bin/bash -c "blkid --match-token TYPE=ext4 /dev/{{.Dev}} || /usr/sbin/mkfs.ext4 /dev/{{.Dev}}"

[Install]
WantedBy=multi-user.target
`

type DiskMountTemplateArgs struct {
	Dev  string
	Name string
}

const diskMountTemplate = `[Unit]
Description=Mount {{.Dev}} disk
After=format-disk-{{.Dev}}.service
DefaultDependencies=no

[Mount]
What=/dev/{{.Dev}}
Where=/var/mnt/{{.Name}}
Type=ext4
Options=defaults

[Install]
WantedBy=multi-user.target
`

type ZtpTemplateArgs struct {
	Username string
	Password string
	Pubkey   string
	TLSCert  string
	TLSKey   string
	Timezone string
}

var ztpTemplate = `#!/bin/bash
function error() {
  echo -e "ERROR: The ZTP script failed while running the command $BASH_COMMAND at line $BASH_LINENO." >&2
  exit 1
}

# Log all output from this script
exec >> /var/log/autoprovision 2>&1
date "+%FT%T ztp starting script $0"

trap error ERR

# Set timezone
timedatectl set-timezone '{{.Timezone}}'

# Set ops username
if ! id {{.Username}} > /dev/null 2>&1; then
	yes | adduser --disabled-password {{.Username}}
	usermod -aG sudo {{.Username}}
fi

# Set ops password
usermod --password $(echo '{{.Password}}' | openssl passwd -1 -stdin) {{.Username}}

# Install ops pubkey in ops user
mkdir -vp /home/{{.Username}}/.ssh
if [ ! -f /home/{{.Username}}/.ssh/authorized_keys ] || ! grep -q '{{.Pubkey}}' /home/{{.Username}}/.ssh/authorized_keys; then
	echo {{.Pubkey}} >> /home/{{.Username}}/.ssh/authorized_keys
fi
chmod 0600 /home/{{.Username}}/.ssh/authorized_keys
chown -R {{.Username}}:{{.Username}} /home/{{.Username}}/.ssh

# Install ops pubkey in root user
mkdir -vp /root/.ssh
if [ ! -f /root/.ssh/authorized_keys ] || ! grep -q '{{.Pubkey}}' /root/.ssh/authorized_keys; then
	echo {{.Pubkey}} >> /root/.ssh/authorized_keys
fi
chmod 0600 /root/.ssh/authorized_keys

# Allow password-less sudo for ops user
if ! (grep -q '{{.Username}}  ALL=(ALL) NOPASSWD: ALL' /etc/sudoers); then
	echo '{{.Username}}  ALL=(ALL) NOPASSWD: ALL' | tee -a /etc/sudoers > /dev/null
fi

mkdir -vp /certs

cat << EOF > /certs/apiserver.pem
{{.TLSCert}}
EOF

cat << EOF > /certs/apiserver-key.pem
{{.TLSKey}}
EOF

### Canopy

systemctl stop canopy@mgmt || true

# Determine arch
arch=$(uname -m)
case $arch in
    armv7l)
      ## GOARM=7 canopy build issues some problematic instructions on armv7l switches with cumulus stock kernel,
      ## so fallback to v5 build
      ##   https://github.com/golang/go/issues/18483
      curl http://ground-control/canopy.arm5 -o /usr/local/bin/canopy
      ;;

    x86_64)
      curl http://ground-control/canopy -o /usr/local/bin/canopy
      ;;

    *)
      echo unknown architecture: $arch
      exit 1
      ;;
esac

chmod +x /usr/local/bin/canopy

cat << EOF > /lib/systemd/system/canopy@.service
[Unit]
Description=Canopy service
After=frr.service
PartOf=frr.service
Requires=frr.service

[Service]
Restart=always
RestartSec=5s
ExecStartPre=/usr/bin/test -f /var/run/frr/watchfrr.started
ExecStart=/usr/local/bin/canopy

[Install]
WantedBy=multi-user.target
EOF

grep -qxF 'canopy' /etc/vrf/systemd.conf || echo 'canopy' >> /etc/vrf/systemd.conf

sed -i 's/^bgpd=no/bgpd=yes/g' /etc/frr/daemons

# start frr before canopy (issue #22)
systemctl daemon-reload
systemctl restart frr
systemctl enable canopy@mgmt
systemctl start canopy@mgmt

# CUMULUS-AUTOPROVISIONING
exit 0
`

var podmanHttpServiceTemplate = `[Unit]
Description=Podman API Service
After=podman.service network-online.target
Wants=podman.service network-online.target

[Service]
ExecStart=/usr/bin/bash -c "podman system service -t 0 tcp::7474"

[Install]
WantedBy=multi-user.target
`

var installServiceTemplate = `[Unit]
Description=Mars Install Service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/local/bin/mars-install
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
`

var FrrDaemons = `# generated by mars install
bgpd=yes
ospfd=no
ospf6d=no
ripd=no
ripngd=no
isisd=no
pimd=no
ldpd=no
nhrpd=no
eigrpd=no
babeld=no
sharpd=no
pbrd=no
bfdd=no
fabricd=no
vrrpd=no

vtysh_enable=yes
zebra_options="  -A 127.0.0.1 -s 90000000"
bgpd_options="   -A 127.0.0.1 -M rpki"
ospfd_options="  -A 127.0.0.1"
ospf6d_options=" -A ::1"
ripd_options="   -A 127.0.0.1"
ripngd_options=" -A ::1"
isisd_options="  -A 127.0.0.1"
pimd_options="   -A 127.0.0.1"
ldpd_options="   -A 127.0.0.1"
nhrpd_options="  -A 127.0.0.1"
eigrpd_options=" -A 127.0.0.1"
babeld_options=" -A 127.0.0.1"
sharpd_options=" -A 127.0.0.1"
pbrd_options="   -A 127.0.0.1"
staticd_options="-A 127.0.0.1"
bfdd_options="   -A 127.0.0.1"
fabricd_options="-A 127.0.0.1"
vrrpd_options="  -A 127.0.0.1"

frr_profile="datacenter"
`

type LinkConfig struct {
	Devname string
	Mtu     uint16
}

var linkProfile = `[connection]
id=infranet-{{.Devname}}
type=ethernet
interface-name={{.Devname}}

[ethernet]
mtu={{.Mtu}}
`

// disable gro/gso on emu nodes due to click performance issues
var emuLinkProfile = linkProfile + `

[ethtool]
feature-gro=false
feature-gso=false
`

type MgmtlinkConfig struct {
	Devname string
}

var mgmtlinkProfile = `[connection]
id=mgmt-{{.Devname}}
type=ethernet
interface-name={{.Devname}}

[ipv4]
dns-search=
method=auto
may-fail=false
`

type PkgInstallTemplateArgs struct {
	Packages []string
}

// recipe cribbed from https://docs.fedoraproject.org/en-US/fedora-coreos/os-extensions/
var pkgInstallTemplate = `[Unit]
Description=Layer additional packages with rpm-ostree
Wants=network-online.target
After=network-online.target
# We run before 'zincati.service' to avoid conflicting rpm-ostree
# transactions.
Before=zincati.service
ConditionPathExists=!/var/lib/%N.stamp

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/rpm-ostree install --apply-live --allow-inactive --idempotent {{range $index, $arg := .Packages}} {{$arg}} {{end}}
ExecStart=/bin/touch /var/lib/%N.stamp

[Install]
WantedBy=multi-user.target
`

type BondMasterConfig struct {
	Devname string
	Mtu     uint16
}

var bondMasterProfile = `[connection]
id=bond-{{.Devname}}
type=bond
interface-name={{.Devname}}

[ethernet]
mtu={{.Mtu}}

[bond]
mode=802.3ad
xmit_hash_policy=layer3+4
miimon=100
lacp_rate=fast

[ipv4]
method=disabled

[ipv6]
method=disabled
`

type BondMemberConfig struct {
	Master  string
	Devname string
	Mtu     uint16
}

var bondMemberProfile = `[connection]
id=bond-member-{{.Devname}}
type=ethernet
interface-name={{.Devname}}
master={{.Master}}
slave-type=bond
zone=trusted

[ethernet]
mtu={{.Mtu}}
`

// disable gro/gso on emu nodes due to click performance issues
var emuBondMemberProfile = bondMemberProfile + `

[ethtool]
feature-gro=false
feature-gso=false
`

var systemdBondPolicy = `
[Match]
Type=bond

[Link]
MACAddressPolicy=none
`
var systemdBridgePolicy = `
[Match]
Type=bridge

[Link]
MACAddressPolicy=none
`

var infraSysctlProfile = `
fs.inotify.max_user_instances = 8192
`

var infrapodNetnsScript = `
#!/bin/bash

if [ $# -lt 2 ]; then
    echo "usage: $(basename $0) <mzid> <command> [<args ...>]"
    echo "e.g., $(basename $0) harbor.system.marstb ping -c 1 172.29.0.1"
    exit 1
fi

# find infractr name from mzid
infractr=$(podman pod inspect $1 | jq -r .InfraContainerID)

# find netns from infractr
netns=$(podman inspect $infractr | jq -r .[].NetworkSettings.SandboxKey | awk -F '/' '{print $4}')

# exec into netns
ip netns exec $netns ${@:2}
`

var reverseProxyCfg = `
#log:
#  level: debug

api:
  insecure: true

entryPoints:
  web:
    address: ":80"

providers:
  etcd:
    endpoints:
    - "etcd:2379"
    rootKey: "traefik"
`

var netemModuleConf = `# load the netem kernel module at boot
sch_netem
`

type CtrlLinkConfig struct {
	Device string
}

var ctrlLinkProfile = `[connection]
id=mgmt-{{.Device}}
interface-name={{.Device}}
type=ethernet

# the ctrl link does not get an address, but rather serves as a
# parent interface for macvlan devices
[ipv4]
method=disabled
`

type MacvlanConfig struct {
	Parent  string // parent device
	MacAddr string // mac addr for macvlan device
}

var macvlanLinkProfile = `[connection]
id=macvlan-macvlan0
type=macvlan
interface-name=macvlan0

[ethernet]
cloned-mac-address={{.MacAddr}}

[macvlan]
mode=2
parent={{.Parent}}

[ipv4]
method=auto

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
`

type ControlNetConfig struct {
	Parent string
}

// goes in /etc/containers/network/mars-control.json
var controlNetTemplate = `{
     "name": "mars-control",
     "id": "5d798f5ec8e47890f8faf0b5ce5d23d540543dcc426b872c93d34a113cf5c324",
     "driver": "macvlan",
     "network_interface": "{{.Parent}}",
     "created": "2024-05-23T22:50:39.71377053Z",
     "subnets": [
          {
               "subnet": "10.0.0.0/16",
               "gateway": "10.0.0.1"
          }
     ],
     "ipv6_enabled": false,
     "internal": false,
     "dns_enabled": false,
     "options": {
          "no_default_route": "true"
     },
     "ipam_options": {
          "driver": "dhcp"
     }
}
`

// goes in /etc/containers/network/mars-gateway.json
var gatewayNetTemplate = `{
     "name": "mars-gateway",
     "id": "6ff13b54731f27f9165654d5d29ed3bae535eac91fd225baa37a267225fcce54",
     "driver": "bridge",
     "network_interface": "gatewaybr",
     "created": "2024-05-23T22:52:34.717108563Z",
     "subnets": [
          {
               "subnet": "10.254.0.0/16",
               "gateway": "10.254.0.1"
          }
     ],
     "routes": [
          {
               "destination": "0.0.0.0/0",
               "gateway": "10.254.0.1",
               "metric": 10
          }
     ],
     "ipv6_enabled": false,
     "internal": false,
     "dns_enabled": false,
     "options": {
          "no_default_route": "true"
     },
     "ipam_options": {
          "driver": "host-local"
     }
}
`
