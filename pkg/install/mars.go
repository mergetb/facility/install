package install

import (
	"bytes"
	"fmt"
	"text/template"

	mrsconfig "gitlab.com/mergetb/facility/mars/pkg/config"
)

// TODO: query ignition?
func SystemdInstallPath() string {
	return "/etc/systemd/system"
}

func TLSCertPath(name string) string {
	switch name {
	case "mars-apiserver":
		return "/var/vol/certs/apiserver.pem"
	case "mars-foundry":
		return "/var/vol/certs/foundry.pem"
	default:
		return ""
	}
}

func TLSKeyPath(name string) string {
	switch name {
	case "mars-apiserver":
		return "/var/vol/certs/apiserver-key.pem"
	case "mars-foundry":
		return "/var/vol/certs/foundry-key.pem"
	default:
		return ""
	}
}

func FrrDaemonsPath() string {
	return "/var/vol/frr/etc/daemons"
}

func BuildContainerSpec(name string, config *mrsconfig.Config) (string, error) {
	spec, err := BuildContainerSpecTemplate(name, config)
	if err != nil {
		return "", err
	}

	return SpecToContents(spec)
}

func BuildContainerSpecTemplate(name string, config *mrsconfig.Config) (*PodmanServiceTemplateArgs, error) {
	var spec *PodmanServiceTemplateArgs

	switch name {
	case "mars-apiserver":
		spec = buildApiserverSpec(config)
	case "mars-canopy":
		spec = buildCanopySpec(config)
	case "mars-infrapod":
		spec = buildInfrapodSpec(config)
	case "mars-mariner":
		spec = buildMarinerSpec(config)
	case "mars-metal":
		spec = buildMetalSpec(config)
	case "mars-wireguard":
		spec = buildWireguardSpec(config)
	case "mars-frr":
		spec = buildFrrSpec(config)
	case "mars-moa":
		spec = buildMoaSpec(config)
	case "mars-pathfinder":
		spec = buildPathfinderSpec(config)
	default:
		return nil, fmt.Errorf("no such container: %s", name)
	}

	return spec, nil
}

func SpecToContents(spec *PodmanServiceTemplateArgs) (string, error) {
	contents, err := serviceTemplateContents(spec)
	if err != nil {
		return "", fmt.Errorf("failed to generate systemd contents: %v", err)
	}
	return contents, nil
}

func serviceTemplateContents(cfg *PodmanServiceTemplateArgs) (string, error) {
	t, err := template.New("podman service").Parse(podmanServiceTemplate)
	if err != nil {
		return "", fmt.Errorf("template parse: %v", err)
	}

	var out bytes.Buffer
	err = t.Execute(&out, cfg)
	if err != nil {
		return "", fmt.Errorf("template execute: %v", err)
	}

	return out.String(), nil
}

func pullOpts(container *mrsconfig.Container) []string {
	opts := []string{}

	if container.Registry.NoVerify {
		opts = append(opts, "--tls-verify=false")
	} else {
		opts = append(opts, "--tls-verify=true")
	}

	if container.Registry.Credentials.Username != "" {
		opts = append(opts,
			fmt.Sprintf("--creds %s:%s",
				container.Registry.Credentials.Username,
				container.Registry.Credentials.Password,
			),
		)
	}

	return opts
}

func buildApiserverSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	iservices := &config.Services.Infraserver
	container := &config.Images.Infraserver.Apiserver

	return &PodmanServiceTemplateArgs{
		Name:  "mars-apiserver",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/var/vol/certs:/certs:Z",
			"-e", fmt.Sprintf("MINIO_ROOT_USER=%s", iservices.Minio.Credentials.Username),
			"-e", fmt.Sprintf("MINIO_ROOT_PASSWORD=%s", iservices.Minio.Credentials.Password),
		),
		PullOptions: pullOpts(container),
	}
}

func buildCanopySpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	container := &config.Images.Infraserver.Canopy

	return &PodmanServiceTemplateArgs{
		Name:  "mars-canopy",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/var/vol/certs:/certs:Z",
			"-v", "/var/vol/frr/run:/var/run/frr:z",
		),
		After:    []string{"mars-frr.service"},
		PartOf:   []string{"mars-frr.service"},
		Requires: []string{"mars-frr.service"},

		PullOptions: pullOpts(container),
		ServiceOptions: []string{
			"ExecStartPre=/usr/bin/test -f /var/vol/frr/run/watchfrr.started",
		},
	}
}

func buildInfrapodSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	iservices := &config.Services.Infraserver
	container := &config.Images.Infraserver.Infrapod

	return &PodmanServiceTemplateArgs{
		Name:  "mars-infrapod",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-e", fmt.Sprintf("MINIO_ROOT_USER=%s", iservices.Minio.Credentials.Username),
			"-e", fmt.Sprintf("MINIO_ROOT_PASSWORD=%s", iservices.Minio.Credentials.Password),
			"-v", "/var/vol:/var/vol:z",
			"--mount", "type=bind,source=/run/netns,destination=/run/netns,bind-propagation=shared",
			"--no-hosts",
		),
		After:       []string{"podman-http.service"},
		PullOptions: pullOpts(container),
		ServiceOptions: []string{
			"ExecStartPre=mkdir -p /run/netns",
		},
	}
}

func buildMarinerSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	container := &config.Images.Infraserver.Mariner

	return &PodmanServiceTemplateArgs{
		Name:  "mars-mariner",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"--pid=host",
			"-v", "mvol:/var/lib/mariner:z",
			"-v", "/run/podman:/run/podman",
		),
		PullOptions: pullOpts(container),
	}
}

func buildMetalSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	iservices := &config.Services.Infraserver
	container := &config.Images.Infraserver.Metal

	return &PodmanServiceTemplateArgs{
		Name:  "mars-metal",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/var/vol/certs:/certs:Z",
			"-e", fmt.Sprintf("MINIO_ROOT_USER=%s", iservices.Minio.Credentials.Username),
			"-e", fmt.Sprintf("MINIO_ROOT_PASSWORD=%s", iservices.Minio.Credentials.Password),
		),
		PullOptions: pullOpts(container),
	}
}

func buildWireguardSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	iservices := &config.Services.Infraserver
	container := &config.Images.Infraserver.Wireguard

	return &PodmanServiceTemplateArgs{
		Name:  "mars-wireguard",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-e", fmt.Sprintf("MINIO_ROOT_USER=%s", iservices.Minio.Credentials.Username),
			"-e", fmt.Sprintf("MINIO_ROOT_PASSWORD=%s", iservices.Minio.Credentials.Password),
			"-v", "/var/vol/certs:/certs:Z",
			"-v", "/run/podman/podman.sock:/run/podman/podman.sock:Z",
			"--mount", "type=bind,source=/run/netns,destination=/run/netns,bind-propagation=shared",
		),
		PullOptions: pullOpts(container),
		ServiceOptions: []string{
			"ExecStartPre=mkdir -p /run/netns",
		},
	}
}

func buildFrrSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	container := &config.Images.Infraserver.Frr

	return &PodmanServiceTemplateArgs{
		Name:  "mars-frr",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/var/vol/frr/run:/var/run/frr:z",
			"-v", "/var/vol/frr/etc/daemons:/etc/frr/daemons:z",
		),
		PullOptions: pullOpts(container),
	}
}

func buildMoaSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	container := &config.Images.Moa.Moa

	return &PodmanServiceTemplateArgs{
		Name:  "mars-moa",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/var/vol/moa:/var/moa",
		),
		PullOptions: pullOpts(container),
	}
}

func buildPathfinderSpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	iservices := &config.Services.Infraserver
	container := &config.Images.Gateway.Pathfinder

	return &PodmanServiceTemplateArgs{
		Name:  "mars-pathfinder",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/run/podman/podman.sock:/run/podman/podman.sock:Z",
			"-e", fmt.Sprintf("MINIO_ROOT_USER=%s", iservices.Minio.Credentials.Username),
			"-e", fmt.Sprintf("MINIO_ROOT_PASSWORD=%s", iservices.Minio.Credentials.Password),
			"-e", "LOGLEVEL=info",
			"--mount", "type=bind,source=/run/netns,destination=/run/netns,bind-propagation=shared",
		),
		PullOptions: pullOpts(container),
	}
}

func buildReverseProxySpec(config *mrsconfig.Config) *PodmanServiceTemplateArgs {
	container := &config.Images.Gateway.ReverseProxy

	return &PodmanServiceTemplateArgs{
		Name:  "mars-reverseproxy",
		Image: container.RegUrl(),
		RunOptions: append(podmanHostOpts,
			"-v", "/etc/traefik:/etc/traefik",
		),
		Args: []string{
			"traefik", "--configFile=/etc/traefik/traefik.yml",
		},
		PullOptions: pullOpts(container),
	}
}
