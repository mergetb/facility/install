package install

import (
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"testing"
)

func TestTemplate(t *testing.T) {
	cfg := new(config.Config)

	// just display specs for sanity checking
	services := []string{
		"mars-apiserver",
		"mars-canopy",
		"mars-infrapod",
		"mars-metal",
		"mars-wireguard",
		"mars-frr",
	}

	for _, name := range services {
		contents, err := BuildContainerSpec(name, cfg)
		if err != nil {
			t.Fatal("Can't generate spec: ", err)
		}
		t.Logf("Service: %s.service\n\n%s\n", name, contents)
	}
}
