package install

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"os"
	"time"

	mrsconfig "gitlab.com/mergetb/facility/mars/pkg/config"
)

func CertInit(config *Config) error {
	// Infrapod services
	{
		c := &config.Mars.Services.Infrapod

		if needsCert(&c.Foundry) {
			cert, key, err := gencert([]string{"foundry"}, "foundry", config)
			if err != nil {
				return err
			}
			c.Foundry.TLS = mrsconfig.TLSGRPCConfig{
				Cert: string(cert),
				Key:  string(key),
			}
		}

		err := writeCert(&c.Foundry, "foundry", installDir)
		if err != nil {
			return err
		}
	}

	// Infraserver services
	{
		c := &config.Mars.Services.Infraserver

		if needsCert(&c.Apiserver) {
			cert, key, err := gencert(
				[]string{"apiserver", "ops." + config.Gc.Domain, "api." + config.Gc.Domain},
				"apiserver",
				config,
			)
			if err != nil {
				return err
			}
			c.Apiserver.TLS = mrsconfig.TLSGRPCConfig{
				Cert: string(cert),
				Key:  string(key),
			}
		}

		err := writeCert(&c.Apiserver, "apiserver", installDir)
		if err != nil {
			return err
		}
	}

	return nil
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
			os.Exit(2)
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func gencert(hosts []string, name string, cfg *Config) ([]byte, []byte, error) {

	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, nil, fmt.Errorf("generate %s key: %v", name, err)
	}
	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"MergeTB"},
			CommonName:   cfg.Gc.Domain,
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(time.Hour * 24 * 365 * 4),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)
	if err != nil {
		return nil, nil, fmt.Errorf("create %s certificate: %v", name, err)
	}

	out := &bytes.Buffer{}
	pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	cert := out.Bytes()

	out = &bytes.Buffer{}
	pem.Encode(out, pemBlockForKey(priv))
	key := out.Bytes()

	return cert, key, nil
}

func writeCert(svc *mrsconfig.ServiceConfig, name, path string) error {
	err := ioutil.WriteFile(
		fmt.Sprintf("%s/%s.pem", path, name),
		[]byte(svc.TLS.Cert),
		0644,
	)
	if err != nil {
		return fmt.Errorf("write certificate %s: %v", name, err)
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("%s/%s-key.pem", path, name),
		[]byte(svc.TLS.Key),
		0600,
	)
	if err != nil {
		return fmt.Errorf("write key %s-key.pem: %v", name, err)
	}

	return nil
}

func needsCert(svc *mrsconfig.ServiceConfig) bool {
	return (svc.TLS.Cert == "" || svc.TLS.Key == "")
}
