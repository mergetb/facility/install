package install

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"text/template"

	util "github.com/coreos/ignition/v2/config/util"
	ignition "github.com/coreos/ignition/v2/config/v3_3/types"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"

	"io/fs"
	"os"

	gcapi "gitlab.com/mergetb/ops/ground-control/pkg/api"
	gcconfig "gitlab.com/mergetb/ops/ground-control/pkg/config"
	"gitlab.com/mergetb/tech/shared/install/cache"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func nextIP(cache *cache.Cache, ip string) (string, error) {
	var err error

	for {
		//log.Debugf("checking if %s is allocated", ip)
		if !cache.IsAllocated(ip) {
			return ip, nil
		}

		ip, err = incip4(ip)
		if err != nil {
			return "", fmt.Errorf("increment ip: %v", err)
		}
	}
}

// cribbed from mars infrapod server
func generateUnicastMacAddr() (string, error) {

	buf := make([]byte, 6)
	_, err := rand.Read(buf)
	if err != nil {
		return "", fmt.Errorf("failed to generate random macaddr bytes: %v", err)
	}

	// ensure local+unicast address
	buf[0] = (buf[0] | 2) & 0xfe

	ar := fmt.Sprintf("%02x:%02x:%02x:%02x:%02x:%02x", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5])
	return ar, nil

}

func Genspec(tbx *xir.Facility, config *Config, zinUpdate, nft bool, cache *cache.Cache) error {

	specs := make(map[string]*ignition.Config)

	gcs := config.Gc
	first, err := firstip4(gcs.Subnet)
	if err != nil {
		return err
	}

	// reserve first 10 IPs
	first, err = incip4By(first, 10)
	if err != nil {
		return err
	}

	var ip string

	for _, r := range tbx.Resources {

		// TB nodes
		if r.HasRole(xir.Role_TbNode) {

			ip = cache.GetNode(r.Id).GetIP()
			if ip == "" {
				ip, err = nextIP(cache, first)
				if err != nil {
					return err
				}
			}

			nc, err := addTbNodeConfig(r, ip)
			if err != nil {
				return err
			}

			if nc != nil {
				gcs.Nodes = append(gcs.Nodes, nc)

				cache.EnsureNode(r.Id).SetIP(ip) // update cache
			}

			// add IPMI if it has one
			ip = cache.GetNode(r.Id).GetIPMI()
			if ip == "" {
				ip, err = nextIP(cache, first)
				if err != nil {
					return err
				}
			}

			ipminc, err := addTbNodeIpmiConfig(r, ip)
			if err != nil {
				return err
			}

			if ipminc != nil {
				gcs.Nodes = append(gcs.Nodes, ipminc)

				cache.EnsureNode(r.Id).SetIPMI(ip) // update cache
			}

			continue
		}

		// switches
		if r.HasRole(xir.Role_InfraSwitch, xir.Role_XpSwitch) {

			ip = cache.GetNode(r.Id).GetIP()
			if ip == "" {
				ip, err = nextIP(cache, first)
				if err != nil {
					return err
				}
			}

			sc, err := addSwitchConfig(r, ip)
			if err != nil {
				return err
			}

			gcs.Switches = append(gcs.Switches, sc)

			cache.EnsureNode(r.Id).SetIP(ip) // update cache

			continue
		}

		// mgmt switches
		if r.HasRole(xir.Role_MgmtSwitch) {
			/*
			   sc, err := addMgmtSwitchConfig(r, ip)
			   if err != nil {
			       return err
			   }

			   gcs.Switches = append(gcs.Switches, sc)

			   ip, err = incip4(ip)
			   if err != nil {
			       return fmt.Errorf("increment ip: %v", err)
			   }
			*/

			continue
		}

		ign, err := initIgnitionConfig(r, nft, config)
		if err != nil {
			return err
		}

		node := cache.EnsureNode(r.Id)
		if node.GetIP() == "" {
			ip, err = nextIP(cache, first)
			if err != nil {
				return err
			}
		} else {
			ip = node.GetIP()
		}

		overwriteNetworkManager(ign)
		setZincatiUpdates(zinUpdate, ign)

		nc := &gcconfig.NodeConfig{
			NodeConfig: &gcapi.NodeConfig{
				Name:     r.Id,
				Ipv4:     ip,
				Ignition: fmt.Sprintf("%s.ign", r.Id),
				Firmware: xirToGcFirmware(r),
			},
		}

		// etcd
		if r.HasRole(xir.Role_EtcdHost) {

			err = addEtcdSpec(r, ign, config)
			if err != nil {
				return err
			}
			nc.Alias = append(nc.Alias, "etcd")

		}

		// minio
		if r.HasRole(xir.Role_MinIOHost) {

			err = addMinIOSpec(r, ign, config)
			if err != nil {
				return err
			}
			nc.Alias = append(nc.Alias, "minio")

		}

		mgmt := r.Mgmt()
		if mgmt == nil {
			return fmt.Errorf("resource %s has no management port", r.Id)
		}
		if mgmt.Mac == "" || mgmt.Mac == "unknown" {
			return fmt.Errorf("resource %s has a management port with no MAC specified", r.Id)
		}

		nc.Mac = mgmt.Mac
		nc.Netdev = r.PortName(mgmt)

		if r.HasRole(xir.Role_NetworkEmulator) {
			// emulation server

			err = addEmulatorConfig(r, ign, config)
			if err != nil {
				return err
			}

		} else if r.HasRole(xir.Role_InfraServer) || r.HasRole(xir.Role_InfrapodServer) {
			// infraserver/infrapod server

			infra := r.Infranet()
			if infra == nil {
				return fmt.Errorf("resource %s has no infranet port", r.Id)
			}
			if infra.Mac == "" || infra.Mac == "unknown" {
				return fmt.Errorf("resource %s has an infranet port with no MAC specified", r.Id)
			}

			if r.HasRole(xir.Role_InfraServer) {
				nc.Alias = append(nc.Alias, "apiserver")

				err = addInfraserverConfig(r, ign, config)
				if err != nil {
					return fmt.Errorf("add infraserver spec to %s: %v", r.Id, err)
				}
			}

			// macvlan device hwaddr
			if node.HwAddr == "" {
				node.HwAddr, err = generateUnicastMacAddr()
				if err != nil {
					return nil
				}
			}

			// infraserver is also an infrapod server
			err = addInfrapodServerConfig(r, ign, config, r.PortName(mgmt), node.HwAddr)
			if err != nil {
				return fmt.Errorf("add infrapod server spec to %s: %v", r.Id, err)
			}

			// add node config for the macvlan device
			gcs.Nodes = append(gcs.Nodes, &gcconfig.NodeConfig{
				NodeConfig: &gcapi.NodeConfig{
					Name: nc.Name,
					Ipv4: nc.Ipv4,
					Mac:  node.HwAddr,
				},
			})

		} else {
			continue
		}

		// Only now do we want to mark this IP as reserved
		node.SetIP(ip)

		for i, x := range r.Disks {
			if x.HasRole(xir.DiskRole_System) {
				nc.Installdisk = "/dev/" + r.DiskName(x)
				log.Infof(
					"Adding system disk for %s at index %d '%s'\n",
					r.Id,
					i,
					nc.Installdisk,
				)
			}
		}
		if nc.Installdisk == "" {
			return fmt.Errorf(
				"resource %s has no system disk specified", r.Id)
		}

		specs[fmt.Sprintf("%s.ign", r.Id)] = ign

		gcs.Nodes = append(gcs.Nodes, nc)

		if node.GetIPMI() != "" {
			ip = node.IPMI
		} else {
			ip, err = nextIP(cache, first)
			if err != nil {
				return err
			}
		}

		ipminc, err := addTbNodeIpmiConfig(r, ip)
		if err != nil {
			return err
		}

		if ipminc != nil {
			gcs.Nodes = append(gcs.Nodes, ipminc)
			node.SetIPMI(ip) // update cache
		}

	}

	err = addSwitchSpecs(config)
	if err != nil {
		return fmt.Errorf("add switch ztp spec: %v", err)
	}

	for name, spec := range specs {

		out, err := json.MarshalIndent(spec, "", "  ")
		if err != nil {
			return fmt.Errorf("marshal spec %s: %v", name, err)
		}

		err = ioutil.WriteFile(fmt.Sprintf("%s/%s", installDir, name), out, 0644)
		if err != nil {
			return fmt.Errorf("write spec %s: %v", name, err)
		}

	}

	// marshal GC config
	out, err := yaml.Marshal(gcs)
	if err != nil {
		return fmt.Errorf("marshal ground control spec %v", err)
	}

	err = os.WriteFile(fmt.Sprintf("%s/ground-control.yml", installDir), out, fs.FileMode(0644))
	if err != nil {
		return fmt.Errorf("write ground control spec %v", err)
	}

	// marshal Mars config
	out, err = yaml.Marshal(config.Mars)
	if err != nil {
		return fmt.Errorf("marshal config: %v", err)
	}

	err = os.WriteFile(generatedConfigFilename, out, fs.FileMode(0644))
	if err != nil {
		return fmt.Errorf("write config: %v", err)
	}

	return nil

}

func xirToGcFirmware(r *xir.Resource) gcapi.PxeFirmware {

	fw := r.GetFirmware()
	if fw == nil {
		log.Warnf("resource %s has no firmware; assuming UEFI", r.Id)
		return gcapi.PxeFirmware_UEFI
	}

	xfw := fw.GetKind()
	switch xfw {
	case xir.Firmware_UEFI:
		return gcapi.PxeFirmware_UEFI
	case xir.Firmware_BIOS:
		return gcapi.PxeFirmware_LEGACY
	default:
		log.Warnf("resource %s has undefined firmware: %d; using UEFI", r.Id, xfw)
		return gcapi.PxeFirmware_UEFI
	}

}

func addTbNodeConfig(r *xir.Resource, ip string) (*gcconfig.NodeConfig, error) {

	mgmt := r.Mgmt()
	if mgmt == nil {
		return nil, fmt.Errorf("resource %s has no management port", r.Id)
	}

	if mgmt.Mac == "" || mgmt.Mac == "unknown" {
		log.Warnf("resource %s has a management port with no MAC specified", r.Id)
		return nil, nil
	}

	return &gcconfig.NodeConfig{
		NodeConfig: &gcapi.NodeConfig{
			Name:     r.Id,
			Ipv4:     ip,
			Mac:      mgmt.Mac,
			Firmware: xirToGcFirmware(r),
		},
	}, nil

}

func addTbNodeIpmiConfig(r *xir.Resource, ip string) (*gcconfig.NodeConfig, error) {

	pc := r.GetPowerControl()
	if pc == nil {
		return nil, fmt.Errorf("resource %s has no power control capability", r.Id)
	}

	// no MAC/IP to register unless IPMI
	bmc := r.GetIpmi()
	if bmc == nil {
		return nil, nil
	}

	host := bmc.GetHost()
	if host == "" {
		return nil, fmt.Errorf("resource %s has IPMI power control but no IPMI hostname", r.Id)
	}

	// add MAC address for IPMI types
	for _, nic := range r.NICs {
		if nic.Kind == xir.NICKind_IPMI {
			mac := nic.Ports[0].Mac
			if mac == "" || mac == "unknown" {
				log.Warnf("resource %s has no IPMI MAC address", r.Id)
				return nil, nil
			}
			// assume first port
			return &gcconfig.NodeConfig{
				NodeConfig: &gcapi.NodeConfig{
					Name:     host,
					Ipv4:     ip,
					Mac:      nic.Ports[0].Mac,
					Firmware: xirToGcFirmware(r),
				},
			}, nil
		}
	}

	log.Warnf("resource %s has IPMI powercontrol but no IPMI NIC", r.Id)
	return nil, nil

}

func addSwitchConfig(r *xir.Resource, ip string) (*gcconfig.SwitchConfig, error) {

	mgmt := r.Mgmt()
	if mgmt == nil {
		return nil, fmt.Errorf("resource %s has no management port", r.Id)
	}

	// switches must have MACs
	if mgmt.Mac == "" {
		return nil, fmt.Errorf("resource %s has a management port with no MAC specified", r.Id)
	}

	ztp := "cumulus-ztp.sh"
	return &gcconfig.SwitchConfig{
		SwitchConfig: &gcapi.SwitchConfig{
			Name: r.Id,
			Ipv4: ip,
			Ztp:  ztp,
			Mac:  mgmt.Mac,
		},
	}, nil

}

func addEmulatorConfig(r *xir.Resource, ign *ignition.Config, config *Config) error {

	log.Infof("Adding network emulator spec to %s", r.Id)

	frrid := 92
	iservices := config.Mars.Services.Infraserver

	addEmbeddedDir("/var/vol/frr/run", ign, frrid, frrid)
	addEmbeddedDir("/var/vol/frr/etc", ign, frrid, frrid)
	addEmbeddedDir("/var/vol/moa", ign, 0, 0)

	addEmbeddedFile(
		FrrDaemonsPath(),
		[]byte(FrrDaemons),
		false,
		0600,
		ign,
	)

	addEmbeddedFile(
		TLSCertPath("mars-apiserver"),
		[]byte(iservices.Apiserver.TLS.Cert),
		false,
		0600,
		ign)

	addEmbeddedFile(
		TLSKeyPath("mars-apiserver"),
		[]byte(iservices.Apiserver.TLS.Key),
		false,
		0600,
		ign)

	addFetchedFile(
		"/usr/local/bin/mars-install",
		"http://ground-control/mars-install",
		0755,
		ign)

	addEmbeddedFile(
		"/etc/modules-load.d/netem.conf",
		[]byte(netemModuleConf),
		false,
		0600,
		ign)

	addInstallSpec(ign)
	addPodmanHttpSpec(ign)

	err := addPackageSpec(
		[]string{
			"lldpd",
			"pciutils",
			"tcpdump",
			"kernel-modules-extra", // for sch_netem
		},
		ign,
	) // nice to have for debugging on the network emulator nodes
	if err != nil {
		return err
	}

	err = addPodmanSystemdSpecs([]*PodmanServiceTemplateArgs{
		buildCanopySpec(config.Mars),
		buildFrrSpec(config.Mars),
		buildMoaSpec(config.Mars),
	}, ign)

	if err != nil {
		return err
	}

	// configure XP link
	bond, ports := bondConfig(r, xir.LinkRole_XpLink)
	if bond != "" {
		err = configureBond(bond, ports, true, ign)
	} else {
		err = configureXpnetLinks(ports, true, ign)
	}

	if err != nil {
		return err
	}

	// mgmt link needs to be configured, otherwise NM will ignore it due to the presence of the above xp link configs
	configureMgmtLink(r.PortName(r.Mgmt()), ign)

	return nil
}

func bondConfig(r *xir.Resource, role xir.LinkRole) (bond string, ports []string) {
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			if p.Role == role {
				if p.GetBond() != nil {
					bond = p.GetBond().Name
				}

				// PhysPortName to bypass the Bond name
				ports = append(ports, r.PhysPortName(p))
			}
		}
	}
	return
}

func initIgnitionConfig(r *xir.Resource, nft bool, config *Config) (*ignition.Config, error) {

	pwHash, err := config.OpsPasswordHash()
	if err != nil {
		return nil, fmt.Errorf("hash ops password: %v", err)
	}

	ign := &ignition.Config{
		Ignition: ignition.Ignition{
			Version: "3.3.0",
		},
		Passwd: ignition.Passwd{
			Users: []ignition.PasswdUser{
				{
					Name: config.Mars.Ops.Username,
					SSHAuthorizedKeys: []ignition.SSHAuthorizedKey{
						ignition.SSHAuthorizedKey(config.Mars.Ops.Ssh.Public),
					},
					Groups:       []ignition.Group{"sudo"},
					PasswordHash: &pwHash,
				},
			},
		},
		KernelArguments: ignition.KernelArguments{
			ShouldExist: []ignition.KernelArgument{
				"iomem=relaxed",
			},
			ShouldNotExist: []ignition.KernelArgument{
				"mitigations=auto,nosmt",
			},
		},
	}

	if nft {
		links := map[string]string{
			"/etc/alternatives/iptables":          "/usr/sbin/iptables-nft",
			"/etc/alternatives/iptables-restore":  "/usr/sbin/iptables-nft-restore",
			"/etc/alternatives/iptables-save":     "/usr/sbin/iptables-nft-save",
			"/etc/alternatives/ip6tables":         "/usr/sbin/ip6tables-nft",
			"/etc/alternatives/ip6tables-restore": "/usr/sbin/ip6tables-nft-restore",
			"/etc/alternatives/ip6tables-save":    "/usr/sbin/ip6tables-nft-save",
		}

		for path, target := range links {
			overwrite := true
			hard := false

			// cannot take addresses of map entries in Go ...
			t := target

			ign.Storage.Links = append(ign.Storage.Links,
				ignition.Link{
					Node: ignition.Node{
						Overwrite: &overwrite,
						Path:      path,
					},
					LinkEmbedded1: ignition.LinkEmbedded1{
						Hard:   &hard,
						Target: &t,
					},
				},
			)
		}
	}

	return ign, nil

}

func addInfraserverConfig(r *xir.Resource, ign *ignition.Config, config *Config) error {

	log.Infof("Adding infraserver spec to %s", r.Id)

	err := addPodmanSystemdSpecs([]*PodmanServiceTemplateArgs{
		buildApiserverSpec(config.Mars),
		//buildCanopySpec(config.Mars),
		//buildInfrapodSpec(config.Mars),
		buildMetalSpec(config.Mars),
		//buildWireguardSpec(config.Mars),
		//buildFrrSpec(config.Mars),
	}, ign)

	return err

}

func fillTemplate(templ string, config any) ([]byte, error) {
	t, err := template.New("fill template").Parse(templ)
	if err != nil {
		return nil, fmt.Errorf("template parse: %v", err)
	}

	var out bytes.Buffer
	err = t.Execute(&out, config)
	if err != nil {
		return nil, fmt.Errorf("template execute: %v", err)
	}

	return []byte(out.String()), nil
}

func addInfrapodServerConfig(r *xir.Resource, ign *ignition.Config, config *Config, portname, macaddr string) error {

	log.Infof("Adding infrapod server spec to %s", r.Id)

	frrid := 92

	addEmbeddedDir("/var/vol/frr/run", ign, frrid, frrid)
	addEmbeddedDir("/var/vol/frr/etc", ign, frrid, frrid)

	iservices := config.Mars.Services.Infraserver
	ipservices := config.Mars.Services.Infrapod

	addEmbeddedFile("/etc/traefik/traefik.yml",
		[]byte(reverseProxyCfg),
		false,
		0644,
		ign)

	addEmbeddedFile(
		TLSCertPath("mars-apiserver"),
		[]byte(iservices.Apiserver.TLS.Cert),
		false,
		0600,
		ign)

	addEmbeddedFile(
		TLSKeyPath("mars-apiserver"),
		[]byte(iservices.Apiserver.TLS.Key),
		false,
		0600,
		ign)

	addEmbeddedFile(
		TLSCertPath("mars-foundry"),
		[]byte(ipservices.Foundry.TLS.Cert),
		false,
		0600,
		ign)

	addEmbeddedFile(
		TLSKeyPath("mars-foundry"),
		[]byte(ipservices.Foundry.TLS.Key),
		false,
		0600,
		ign)

	addEmbeddedFile(
		FrrDaemonsPath(),
		[]byte(FrrDaemons),
		false,
		0600,
		ign,
	)

	addEmbeddedFile("/etc/sysctl.d/50-merge.conf",
		[]byte(infraSysctlProfile),
		false,
		0644,
		ign)

	addFetchedFile(
		"/usr/local/bin/mars-install",
		"http://ground-control/mars-install",
		0755,
		ign)

	out, err := fillTemplate(controlNetTemplate, ControlNetConfig{Parent: portname})
	if err != nil {
		return err
	}
	addEmbeddedFile(
		"/etc/containers/networks/mars-control.json",
		out,
		false,
		0600,
		ign)

	addEmbeddedFile(
		"/etc/containers/networks/mars-gateway.json",
		[]byte(gatewayNetTemplate),
		false,
		0600,
		ign)

	out, err = fillTemplate(ctrlLinkProfile, CtrlLinkConfig{Device: portname})
	if err != nil {
		return err
	}
	addEmbeddedFile(
		"/etc/NetworkManager/system-connections/mgmt-"+portname+".nmconnection",
		out,
		false,
		0600,
		ign)

	out, err = fillTemplate(macvlanLinkProfile, MacvlanConfig{
		Parent:  portname,
		MacAddr: macaddr,
	})
	if err != nil {
		return err
	}
	addEmbeddedFile(
		"/etc/NetworkManager/system-connections/macvlan-macvlan0.nmconnection",
		out,
		false,
		0600,
		ign)

	addSystemdService("netavark-dhcp-proxy.socket", ign)
	addPodmanHttpSpec(ign)
	addInfrapodNetnsSpec(ign)
	addInstallSpec(ign)

	err = addPodmanSystemdSpecs([]*PodmanServiceTemplateArgs{
		buildCanopySpec(config.Mars),
		buildInfrapodSpec(config.Mars),
		buildWireguardSpec(config.Mars),
		buildFrrSpec(config.Mars),
		buildPathfinderSpec(config.Mars),   // should really be in Role_Gateway
		buildReverseProxySpec(config.Mars), // should really be in Role_Gateway
	}, ign)

	if err != nil {
		return err
	}

	bond, ports := bondConfig(r, xir.LinkRole_InfraLink)
	if bond != "" {
		err = configureBond(bond, ports, false, ign)
	} else {
		if len(ports) != 1 {
			return fmt.Errorf("resource %s has # infra ports != 1 and not bonded", r.Id)
		}
		err = configureInfranetLink(ports[0], ign)
	}

	return err

}

func addMinIOSpec(r *xir.Resource, ign *ignition.Config, config *Config) error {

	var disks []string
	for _, x := range r.Disks {
		if x.HasRole(xir.DiskRole_MinIO) {
			//disks = append(disks, x.DevName(uint(i)))
			disks = append(disks, r.DiskName(x))
		}
	}

	if len(disks) == 0 {
		return fmt.Errorf("resource %s has 0 disks with the MinIO role", r.Id)
	}

	afters := []string{}
	volpaths := []string{}

	opts := podmanHostOpts
	dirMode := 0744
	iservices := config.Mars.Services.Infraserver

	for i, d := range disks {
		name := fmt.Sprintf("minio%d", i)
		opts = append(opts,
			"-v", fmt.Sprintf("/var/mnt/%s:/%s", name, name),
			"-e", fmt.Sprintf("MINIO_ROOT_USER=%s", iservices.Minio.Credentials.Username),
			"-e", fmt.Sprintf("MINIO_ROOT_PASSWORD=%s", iservices.Minio.Credentials.Password),
		)

		diskFormatSvc, err := addDiskFormatSpec(d, ign)
		if err != nil {
			return fmt.Errorf("add disk %s format spec: %v", d, err)
		}

		diskMountSvc, err := addDiskMountSpec(d, name, ign)
		if err != nil {
			return fmt.Errorf("add disk %s mount spec: %v", d, err)
		}

		afters = append(afters, diskFormatSvc, diskMountSvc)
		volpaths = append(volpaths, fmt.Sprintf("/%s", name))

		ign.Storage.Directories = append(ign.Storage.Directories, ignition.Directory{
			Node: ignition.Node{
				Path: fmt.Sprintf("/var/mnt/%s", name),
			},
			DirectoryEmbedded1: ignition.DirectoryEmbedded1{
				Mode: &dirMode,
			},
		})
	}

	return addPodmanSystemdSpecs([]*PodmanServiceTemplateArgs{
		{
			Name:        "mars-minio",
			Image:       config.Mars.Images.Infraserver.Minio.RegUrl(),
			RunOptions:  opts,
			PullOptions: pullOpts(&config.Mars.Images.Infraserver.Minio),
			After:       afters,
			Args:        append([]string{"minio", "server"}, volpaths...),
		},
	}, ign)

}

func addEtcdSpec(r *xir.Resource, ign *ignition.Config, config *Config) error {

	var disk string
	for _, x := range r.Disks {
		if x.HasRole(xir.DiskRole_Etcd) {
			//disk = x.DevName(uint(i))
			disk = r.DiskName(x)
			break
		}
	}
	if disk == "" {
		return fmt.Errorf("resource %s has 0 disks with the Etcd role", r.Id)
	}

	opts := podmanHostOpts

	opts = append(opts,
		"-v", "/var/mnt/etcd:/etcd",
		"-e", "ETCD_LISTEN_CLIENT_URLS=http://0.0.0.0:2379",
		"-e", "ETCD_ADVERTISE_CLIENT_URLS=http://etcd:2379",
		"-e", "ETCD_MAX_TXN_OPS=32678",
		"-e", "ETCD_MAX_REQUEST_BYTES=536870912",
		"-e", "ETCD_DATA_DIR=/etcd",
		"-e", "ETCD_AUTO_COMPACTION_MODE=revision",
		"-e", "ETCD_AUTO_COMPACTION_RETENTION=1000",
		"-e", "ETCD_SNAPSHOT_COUNT=100",
		"-e", "ETCD_QUOTA_BACKEND_BYTES=8000000000",
	)

	diskFormatSvc, err := addDiskFormatSpec(disk, ign)
	if err != nil {
		return fmt.Errorf("add disk %s format spec: %v", disk, err)
	}

	diskMountSvc, err := addDiskMountSpec(disk, "etcd", ign)
	if err != nil {
		return fmt.Errorf("add disk %s mount spec: %v", disk, err)
	}

	afters := []string{diskFormatSvc, diskMountSvc}
	dirMode := 0744

	ign.Storage.Directories = append(ign.Storage.Directories, ignition.Directory{
		Node: ignition.Node{
			Path: "/var/mnt/etcd",
		},
		DirectoryEmbedded1: ignition.DirectoryEmbedded1{
			Mode: &dirMode,
		},
	})

	return addPodmanSystemdSpecs([]*PodmanServiceTemplateArgs{
		{
			Name:        "mars-etcd",
			Image:       config.Mars.Images.Infraserver.Etcd.RegUrl(),
			RunOptions:  opts,
			PullOptions: pullOpts(&config.Mars.Images.Infraserver.Etcd),
			After:       afters,
		},
	}, ign)

}

func addPodmanSystemdSpecs(cfgs []*PodmanServiceTemplateArgs, ign *ignition.Config) error {

	for _, cfg := range cfgs {

		err := addPodmanSystemdSpec(cfg, ign)
		if err != nil {
			return fmt.Errorf("add %s spec: %v", cfg.Name, err)
		}

	}

	return nil

}

func addPodmanSystemdSpec(cfg *PodmanServiceTemplateArgs, ign *ignition.Config) error {

	content, err := serviceTemplateContents(cfg)
	if err != nil {
		return fmt.Errorf("service template to string: %v", err)
	}

	addSystemdSpec(fmt.Sprintf("%s.service", cfg.Name), content, ign)
	return nil

}

func addSystemdSpec(name, contents string, ign *ignition.Config) {

	enabled := true

	ign.Systemd.Units = append(ign.Systemd.Units, ignition.Unit{
		Name:     name,
		Enabled:  &enabled,
		Contents: &contents,
	})

}

// enable an existing service
func addSystemdService(name string, ign *ignition.Config) {

	enabled := true

	ign.Systemd.Units = append(ign.Systemd.Units, ignition.Unit{
		Name:    name,
		Enabled: &enabled,
	})

}

func addPodmanHttpSpec(ign *ignition.Config) {

	addSystemdSpec("podman-http.service", podmanHttpServiceTemplate, ign)

}

func addInstallSpec(ign *ignition.Config) {

	addSystemdSpec("mars-install.service", installServiceTemplate, ign)

}

func addInfrapodNetnsSpec(ign *ignition.Config) {

	addEmbeddedFile("/usr/local/sbin/infrapod-netns-exec", []byte(infrapodNetnsScript), true, 0755, ign)

}

// uses rpm-ostree to install extra fcos packages on first boot
func addPackageSpec(pkgs []string, ign *ignition.Config) error {

	t, err := template.New("package install service").Parse(pkgInstallTemplate)
	if err != nil {
		return fmt.Errorf("template parse: %v", err)
	}

	cfg := PkgInstallTemplateArgs{Packages: pkgs}

	var out bytes.Buffer
	err = t.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template execute: %v", err)
	}

	service := out.String()
	addSystemdSpec("rpm-ostree-install.service", service, ign)

	return nil
}

func addDiskFormatSpec(dev string, ign *ignition.Config) (string, error) {

	t, err := template.New("disk format service").Parse(diskFormatTemplate)
	if err != nil {
		return "", fmt.Errorf("template parse: %v", err)
	}

	cfg := DiskFormatTemplateArgs{Dev: dev}

	var out bytes.Buffer
	err = t.Execute(&out, cfg)
	if err != nil {
		return "", fmt.Errorf("template execute: %v", err)
	}

	service := out.String()
	name := fmt.Sprintf("format-disk-%s.service", dev)
	addSystemdSpec(name, service, ign)

	return name, nil

}

func addDiskMountSpec(dev, name string, ign *ignition.Config) (string, error) {

	t, err := template.New("disk mount service").Parse(diskMountTemplate)
	if err != nil {
		return "", fmt.Errorf("template parse: %v", err)
	}

	cfg := DiskMountTemplateArgs{Dev: dev, Name: name}

	var out bytes.Buffer
	err = t.Execute(&out, cfg)
	if err != nil {
		return "", fmt.Errorf("template execute: %v", err)
	}

	service := out.String()
	svcname := fmt.Sprintf("var-mnt-%s.mount", name)
	addSystemdSpec(svcname, service, ign)

	return svcname, nil

}

func addEmbeddedFile(path string, content []byte, overwrite bool, mode int, ign *ignition.Config) {

	data := base64.StdEncoding.EncodeToString(content)
	source := fmt.Sprintf("data:text/plain;charset=utf-8;base64,%s", data)

	ign.Storage.Files = append(ign.Storage.Files, ignition.File{
		Node: ignition.Node{
			Path:      path,
			Overwrite: util.BoolToPtr(overwrite),
		},
		FileEmbedded1: ignition.FileEmbedded1{
			Contents: ignition.Resource{
				Source: &source,
			},
			Mode: &mode,
		},
	})

}

func addFetchedFile(path, url string, mode int, ign *ignition.Config) {

	ign.Storage.Files = append(ign.Storage.Files, ignition.File{
		Node: ignition.Node{
			Path: path,
		},
		FileEmbedded1: ignition.FileEmbedded1{
			Contents: ignition.Resource{
				Source: &url,
			},
			Mode: &mode,
		},
	})

}

func addEmbeddedDir(path string, ign *ignition.Config, user, group int) {

	ign.Storage.Directories = append(ign.Storage.Directories, ignition.Directory{
		Node: ignition.Node{
			Path: path,
			Group: ignition.NodeGroup{
				ID: &group,
			},
			User: ignition.NodeUser{
				ID: &user,
			},
		},
	})

}

func addSwitchSpecs(config *Config) error {

	cfg := ZtpTemplateArgs{
		Username: config.Mars.Ops.Username,
		Password: config.Mars.Ops.Password,
		Pubkey:   strings.TrimSuffix(config.Mars.Ops.Ssh.Public, "\n"),
		TLSCert:  string(config.Mars.Services.Infraserver.Apiserver.TLS.Cert),
		TLSKey:   string(config.Mars.Services.Infraserver.Apiserver.TLS.Key),
		Timezone: "America/Los_Angeles",
	}

	return addSwitchSpec("cumulus-ztp.sh", cfg)

}

func addSwitchSpec(filename string, cfg ZtpTemplateArgs) error {

	t, err := template.New("ztp").Parse(ztpTemplate)
	if err != nil {
		return fmt.Errorf("template parse: %v", err)
	}

	var out bytes.Buffer
	err = t.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template execute: %v", err)
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("%s/%s", installDir, filename),
		out.Bytes(),
		0644,
	)
	if err != nil {
		return fmt.Errorf("write error: %v", err)
	}

	return nil

}

func overwriteNetworkManager(ign *ignition.Config) {
	/*
		https://developer.gnome.org/NetworkManager/stable/NetworkManager.conf.html
		tl;dr: Specify devices for which NetworkManager shouldn't create default wired connection (Auto eth0).
	*/
	nmPath := "/etc/NetworkManager/NetworkManager.conf"
	mainSettings := "[main]\nno-auto-default=*\n"

	addEmbeddedFile(nmPath, []byte(mainSettings), true, 0600, ign)
}

func configureBond(bond string, ports []string, emu bool, ign *ignition.Config) error {

	// bond interface
	path := fmt.Sprintf("/etc/NetworkManager/system-connections/%s.nmconnection", bond)

	t, err := template.New("bond master profile").Parse(bondMasterProfile)
	if err != nil {
		return fmt.Errorf("bond master profile template parse: %v", err)
	}

	var out bytes.Buffer
	err = t.Execute(&out, BondMasterConfig{Mtu: 9216, Devname: bond})
	if err != nil {
		return fmt.Errorf("bond master profile template exec: %v", err)
	}

	addEmbeddedFile(path, out.Bytes(), true, 0600, ign)

	// member interfaces
	for _, p := range ports {
		path := fmt.Sprintf("/etc/NetworkManager/system-connections/bond-member-%s.nmconnection", p)

		var t *template.Template

		if emu {
			t, err = template.New("emu bond member profile").Parse(emuBondMemberProfile)
			if err != nil {
				return fmt.Errorf("emu bond member profile template parse: %v", err)
			}
		} else {
			t, err = template.New("bond member profile").Parse(bondMemberProfile)
			if err != nil {
				return fmt.Errorf("bond member profile template parse: %v", err)
			}
		}

		var out bytes.Buffer
		err = t.Execute(&out, BondMemberConfig{Mtu: 9216, Devname: p, Master: bond})
		if err != nil {
			return fmt.Errorf("bond master profile template exec: %v", err)
		}

		addEmbeddedFile(path, out.Bytes(), true, 0600, ign)
	}

	// systemd MAC address policy fix for bonds
	addEmbeddedFile(
		"/etc/systemd/network/98-bond-mac.link",
		[]byte(systemdBondPolicy),
		true,
		0644,
		ign,
	)

	return nil

}

func configureXpnetLinks(ports []string, emu bool, ign *ignition.Config) error {

	for _, p := range ports {
		path := fmt.Sprintf("/etc/NetworkManager/system-connections/xplink-%s.nmconnection", p)

		var t *template.Template
		var err error

		if emu {
			t, err = template.New("emu link profile").Parse(emuLinkProfile)
			if err != nil {
				return fmt.Errorf("emu link profile template parse: %v", err)
			}
		} else {
			t, err = template.New("link profile").Parse(linkProfile)
			if err != nil {
				return fmt.Errorf("link profile template parse: %v", err)
			}
		}

		var out bytes.Buffer
		err = t.Execute(&out, LinkConfig{Mtu: 9216, Devname: p})
		if err != nil {
			return fmt.Errorf("xplink profile template exec: %v", err)
		}

		addEmbeddedFile(path, out.Bytes(), true, 0600, ign)
	}

	return nil

}

func configureInfranetLink(portname string, ign *ignition.Config) error {

	path := fmt.Sprintf("/etc/NetworkManager/system-connections/infralink-%s.nmconnection", portname)

	t, err := template.New("link profile").Parse(linkProfile)
	if err != nil {
		return fmt.Errorf("link profile template parse: %v", err)
	}

	var out bytes.Buffer
	err = t.Execute(&out, LinkConfig{Mtu: 9216, Devname: portname})
	if err != nil {
		return fmt.Errorf("infralink profile template exec: %v", err)
	}

	addEmbeddedFile(path, out.Bytes(), true, 0600, ign)

	return nil

}

func configureMgmtLink(portname string, ign *ignition.Config) error {

	path := fmt.Sprintf("/etc/NetworkManager/system-connections/mgmt-%s.nmconnection", portname)

	t, err := template.New("mgmtlink profile").Parse(mgmtlinkProfile)
	if err != nil {
		return fmt.Errorf("mgmtlink profile template parse: %v", err)
	}

	var out bytes.Buffer
	err = t.Execute(&out, MgmtlinkConfig{Devname: portname})
	if err != nil {
		return fmt.Errorf("mgmtlink profile template exec: %v", err)
	}

	addEmbeddedFile(path, out.Bytes(), true, 0600, ign)

	return nil

}

func setZincatiUpdates(allowUpdate bool, ign *ignition.Config) {

	/*
		https://github.com/coreos/zincati/blob/main/docs/usage/auto-updates.md
		tl;dr add [updates]\nenabled = false to disable updates
		also can do [identity]\nrollout_wariness = .5 (0 very, 1 cautious) to have canary rollout nodes
	*/
	zcPath := "/etc/zincati/config.d/90-disable-auto-updates.toml"
	updateSettings := fmt.Sprintf("[updates]\nenabled = %t\n", allowUpdate)

	addEmbeddedFile(zcPath, []byte(updateSettings), true, 0644, ign)
}
