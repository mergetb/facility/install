CMDDIR=cmd/facility-install
BUILDDIR=$(shell pwd)/build
GOFILES=$(CMDDIR)/*.go pkg/install/*.go
TARGET=$(BUILDDIR)/facility-install
	
all: $(TARGET)

$(TARGET): $(GOFILES)
	cd $(CMDDIR) && CGO_ENABLED=0 go build -v -o $@

.PHONY: clean
clean:
	rm -f $(TARGET)
