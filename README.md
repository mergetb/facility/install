# facility-install

This program creates files to provosion a Merge TestBed (MergeTB) facility in a directory called "merge-install".

It takes an optional input file `./user-config.yml`.

## usage

`generate <path to model.xir>`

:   Generate base facility configuration files according to the XIR model.

`-h, --help, help`

:   Display help messages.

` completion <shell>`

:   Generate autocompletion scripts for available shells.

## building

If you are on a system with GNU `make` and `golang` (Go language tools) installed, you should simply be able to run `make` to generate a `./build/facility-install` static binary.

## installing

### packages
#### RPM
**Do either 1 or 2:**
1. Install the MergeTB repo for your rpm-supporting distro if it is available (check https://pkg.mergetb.org/rpms), and `yum|dnf install facility-install` to download and install the signed package
  
   OR
2. Download and install the unsigned RPM from GitLab. Check this code repo's "Generic Package" repository for the latest build artifacts. (https://gitlab.com/mergetb/facility/install/-/packages)

### pre-built static binary
Download the latest build artifacts from GitLab Pipelines from this code repo, or grab the latest version from the [Generic Package repo](https://gitlab.com/mergetb/facility/install/-/packages) in this code repo. You will want to verify that the binary is executable by users on your system and place it into the system PATH.


### self-built static binary
If you manually build using Go/Make, you should just verify that all users on your system can execute the static binary and place it into system PATH.
